# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/jaxb-api/issues
# Bug-Submit: https://github.com/<user>/jaxb-api/issues/new
# Changelog: https://github.com/<user>/jaxb-api/blob/master/CHANGES
# Documentation: https://github.com/<user>/jaxb-api/wiki
# Repository-Browse: https://github.com/<user>/jaxb-api
# Repository: https://github.com/<user>/jaxb-api.git
