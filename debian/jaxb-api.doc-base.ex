Document: jaxb-api
Title: Debian jaxb-api Manual
Author: <insert document author here>
Abstract: This manual describes what jaxb-api is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/jaxb-api/jaxb-api.sgml.gz

Format: postscript
Files: /usr/share/doc/jaxb-api/jaxb-api.ps.gz

Format: text
Files: /usr/share/doc/jaxb-api/jaxb-api.text.gz

Format: HTML
Index: /usr/share/doc/jaxb-api/html/index.html
Files: /usr/share/doc/jaxb-api/html/*.html
